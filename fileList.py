import os
import datetime

info="""
A simple utility tool to list to-be-published news items in a file. 
The first version was written in python 2.7 back in 2015-16 at
Daily Observer newsroom. 
Ishtiaque Foysol
"""

output = open("newsList.txt", "w+")
dTime = datetime.datetime.now().strftime("%d/%m/%Y  %H:%M:%S\n")
path = input("Input Folder Location: ")

stories = []
for direc in os.listdir(path):
    if os.path.isfile(os.path.join(path, direc)):
        stories.append(direc)

output.write(dTime)
output.write("City Desk: \n")

for story in stories:
    output.write(story)
    output.write("\n")
output.close()
