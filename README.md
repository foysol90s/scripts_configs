# scripts_configs

A collection of my hand-rolled day-to-day use scripts and config files.

**Description**

- fileList.py -- a small python script to list files in a directory
- hddcron -- a basic cpu/hdd monitoring cronjob tool mainly wrote for a 24/7 rpi2 torrent client.
- imdb_scrape -- an interactive python script to search imdb without a browser.
- millbill -- a million billion confusion cracker in newsroom.
- nmapSH --an interactive bash wrapper for my most commonly used nmap commands.
- package_checklist -- a simple bash script for finding frequently used binaries in a system after a fresh install.
- read_bookmarks -- a simple python script to retrieve bookmarks from firefox, before a fresh install 
- readNmap -- a basic network monitoring bash script mainly written for an rpi torrentbox for scanning LAN and detecting unknown device(s) [if any].
- scrape_bbc -- very basic and self explanatory bbc news scraper in python.
- scrape_bdnews -- very basic and self explanatory bdnews24 scraper in python.
- snapsh -- a simple bash script, collection of my frequently used commands after login into a personal box.
- templater -- A simple bash utility to generate a quick file with custom template for daily use. 
